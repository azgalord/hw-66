import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Header from "./components/Header/Header";
import Add from "./containers/Add/Add";
import Home from "./containers/Home/Home";
import PostInfo from "./containers/PostInfo/PostInfo";
import Edit from "./containers/Edit/Edit";

import './App.css';
import About from "./containers/About/About";
import Contact from "./containers/Contacts/Contact";

class App extends Component {
  state = {
    urls: [
      {path: '/', title: 'home',exact:true},
      {path: '/add', title: 'add',exact:true},
      {path: '/about', title: 'about',exact:true},
      {path: '/contacts', title: 'contacts',exact:true}
    ],
  };

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <Header links={this.state.urls}/>
            <Switch>
              <Route path="/" exact component={Home}/>
              <Route path="/add" component={Add}/>
              <Route path="/posts/:id" exact component={PostInfo}/>
              <Route path="/posts/:id/edit" exact component={Edit}/>
              <Route path="/about" component={About}/>
              <Route path='/contacts' component={Contact}/>
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
