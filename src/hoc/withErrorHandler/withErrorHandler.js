import React, {Component, Fragment} from 'react';
import Spinner from '../../components/UI/Spinner/Spinner';

const withErrorHandler = (WrappedComponent, axios) => {
  return class WithErrorHOC extends Component {

    constructor(props) {
      super(props);

      this.state = {
        loading: false,
        error: null
      };

      this.state.reqInterceptorId = axios.interceptors.request.use(req => {
        this.setState({loading: true});
        return req;
      });

      this.state.interceptorsId = axios.interceptors.response.use(res => {
        this.setState({loading: false});
        return res;
      }, error => {
        this.setState({error});
        throw error;
      });
    }

    componentWillUnmount() {
      axios.interceptors.response.eject(this.state.interceptorsId);
      axios.interceptors.request.eject(this.state.reqInterceptorId);
    }

    render() {
      return (
        <Fragment>
          {this.state.loading ? <Spinner/> : null}
          <WrappedComponent {...this.props}/>
        </Fragment>
      )
    }
  }
};

export default withErrorHandler;
