import React, {Component} from 'react';
import axios from '../../axios-blog';
import LinkButton from "../../components/Post/LinkButton/LinkButton";
import Delete from "./Delete/Delete";
import Container from "../../components/Container/Container";

import './PostInfo.css';
class PostInfo extends Component{
  state = {
    title: '',
    date: '',
    body: ''
  };

  getId = () => {
    return '/blog/' + this.props.match.params.id + '.json';
  };

  componentDidMount() {
    axios.get(this.getId()).then(response => {
      const data = response.data;
      this.setState({title: data.title, date: data.date, body: data.description});
    })
  }

  deletePost = () => {
    axios.delete(this.getId()).then(() => {
      this.props.history.replace('/');
    })
  };

  render() {
    return (
        <Container>
        <div className="PostInfo">
            Post Information
            <h2 className='PostInfoTitle'>{this.state.title}</h2>
            <span className='PostInfoDate'>{this.state.date}</span>
            <p className='PostInfoBody'>{this.state.body}</p>
            <LinkButton href={'/posts/' + this.props.match.params.id + '/edit'} text="Edit"/>
            <Delete delete={() => this.deletePost()}/>
        </div>
        </Container>
    );
  }
}

export default PostInfo;