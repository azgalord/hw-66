import React, {Component} from 'react';
import axios from '../../axios-blog';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Post from "../../components/Post/Post";
import Container from "../../components/Container/Container";

import './Home.css';

class Home extends Component {
  state = {
    posts: null,
    loading: false
  };

  componentDidMount() {
    axios.get('/blog.json').then((response) => {
      const posts = Object.values(response.data);
      const keys = Object.keys(response.data);

      for (let key in posts) {
        posts[key].id = keys[key];
      }

      this.setState({posts});
    })
  }

  render() {

    let posts = () => {
      if (this.state.posts) {
        return this.state.posts.map((post) => (
            <Post
                date={post.date}
                body={post.description}
                key={post.id}
                href={'/posts/' + post.id}
            />
        ))
      } else {
        return (<div>No posts here...</div>)
      }
    };

    return (
        <div className="Home">
          <Container>
            {posts()}
          </Container>
        </div>
    );
  }
}

export default withErrorHandler(Home, axios);
