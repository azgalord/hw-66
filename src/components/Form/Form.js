import React from 'react';

import './Form.css';

const Form = props => {
  return (
      <div className='Form'>
        <h4>{props.formTitle}</h4>
        <form className="FormBlock">
          <label htmlFor="title">Title</label>
          <input
              value={props.title} onChange={props.changeValue}
              name="title" type="text" id="title"
          />
          <label htmlFor="description">Description</label>
          <textarea value={props.body} onChange={props.changeValue}
                    name="description" id="description" cols="30" rows="10"
          />
          <button onClick={props.sendPost} type="submit">{props.button}</button>
        </form>
      </div>
  );
};

export default Form;