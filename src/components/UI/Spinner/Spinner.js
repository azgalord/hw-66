import React from 'react';

import './Spinner.css';

const Spinner = () => {
  return (
    <div className="SpinnerContainer">
      <div className="Spinner">Loading...</div>
    </div>
  );
};

export default Spinner;
