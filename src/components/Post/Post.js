import React from 'react';

import './Post.css';
import LinkButton from "./LinkButton/LinkButton";

const Post = props => {
  return (
      <div className="Post">
        <span className="PostDate">{props.date}</span>
        <p className="PostBody">{props.body}</p>
        <LinkButton href={props.href} text="Read More"/>
      </div>
  );
};

export default Post;