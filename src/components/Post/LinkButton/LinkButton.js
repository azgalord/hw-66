import React from 'react';

import { Link } from 'react-router-dom';

const LinkButton = props => {
  return (
      <Link to={props.href}>{props.text}</Link>
  );
};

export default LinkButton;