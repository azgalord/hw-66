import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://posts-azamat.firebaseio.com/'
});

export default instance;